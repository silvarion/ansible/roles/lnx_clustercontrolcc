# Ansible Role Linux Cluster Control

## General Information

**Author:**     Jesus Alejandro Sanchez Davila

**Maintainer:** Jesus Alejandro Sanchez Davila

**Email:**      [jsanchez.consultant@gmail.com](mailto:jsanchez.consultant@gmail.com)

## Description

This role installs Several 9s Cluster Control Community Edition on the Linux distro that is run on.

This role was created with `molecule` with the Docker driver to allow for pre-testing. It can be added/installed via ansible-galaxy to make use of it as a building block from complex playbooks.

## Supported Platforms

This role has been tested on:

- _Alma Linux 8_
- _Amazon Linux 2_
- _Amazon Linux 2023_
- _CEntOS 7_
- _Opensuse Leap 15_
- _Rocky Linux 8_
- _Ubuntu 20.04_
- _Ubuntu 22.04_

### Notes

Systemd tasks not fully tested

## Usage

### Role Requirements File

This role can be added to a `requirements.yml` file as follows

    # Silvarion's Linux Cluster Control Role
    - src: git@gitlab.com:silvarion/ansible/roles/lnx_clustercontrolcc.git
      scm: git
      version: "main"  # quoted, so YAML doesn't parse this as a floating-point value

Then run `ansible-galaxy install -r requirements.yml`

### Playbook Additional Task

An addirional task can also be added in a playbook to install the role locally before including the role in the playbook itself

    ---
    - name: Sample Playbook
      hosts: all
      gather_facts: true
      # ...

      collections:
        # All the collections you'll need

      pre_tasks:
        - name: Install requirements
          delegate_to: localhost
          ansible.builtin.command: ansible-galaxy install -r {{ role_path }}/requirements.yml # <-- This is your requirements.yml file
        # Other tasks that you run BEFORE the roles

      roles:
        - { role: "silvarion.lnx_clustercontrolcc" }
        # your other roles

      post_tasks:
        # other tasks that you run AFTER the roles
